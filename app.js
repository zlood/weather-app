const yargs = require('yargs')
const axios = require('axios')
const fconv = require('./weather/fahrenheit-converter')

const argv = yargs
  .options({
    a: {
      alias: 'address',
      describe: 'Address you are looking for. Example: \'Depok Indonesia\' ',
      string: true
    }
  })
  .help()
  .alias('help','h')
  .argv

const encodedAddress = argv.address ? encodeURIComponent(argv.address) : encodeURIComponent('Duren Sawit Jakarta')
const geocodeUrl = `http://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`
let formattedAddress

axios.get(geocodeUrl)
  .then((response) => {
    if (response.data.status === 'ZERO_RESULTS') {
      throw new Error('Address unknown.')
    } else if (response.data.status === 'OVER_QUERY_LIMIT') {
      throw new Error('You have reached google map API hit limit. Please wait a few minutes...')
    }
    const lat = response.data.results[0].geometry.location.lat
    const lng = response.data.results[0].geometry.location.lng
    formattedAddress = response.data.results[0].formatted_address
    const weatherUrl = `https://api.darksky.net/forecast/bc7748dc193fe98c1815007ea90991af/${lat},${lng}`
    return axios.get(weatherUrl)
  })
  .then((response) => {
    if (response.status === 400) {
      throw new Error('Unable to fetch weather')
    }
    const temperature = Number(fconv.convertTemp(response.data.currently.temperature))
    const actualTemperature = Number(fconv.convertTemp(response.data.currently.apparentTemperature))
    console.log(`current temperature in ${formattedAddress} is ${temperature}C. But it feels like ${actualTemperature}C`);
  })
  .catch((e) => {
    if (e.code === 'ENOTFOUND') {
      console.log('Unable to connect to servers')
    } else {
      console.log(e.message);
    }
  })
