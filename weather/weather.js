const request = require('request');
const fconv = require('./fahrenheit-converter');

var getWeather = (lat,lng,callback) => {
  request({
    url: `https://api.darksky.net/forecast/bc7748dc193fe98c1815007ea90991af/${lat},${lng}`,
    json: true
  }, (error, response, body) => {
    if (error) {
      callback('can\'t communicate forecast.io server');
    } else if (response.statusCode === 400) {
      callback('unable to fetch weather');
    } else if (response.statusCode === 200) {
      callback(undefined, {
        temperature: Number(fconv.convertTemp(body.currently.temperature)),
        actualTemperature: Number(fconv.convertTemp(body.currently.apparentTemperature))
      })
    }
  })
}

module.exports.getWeather = getWeather;
