var convertTemp = temp => {
  return Number((temp - 32) * 5 / 9).toFixed(2)
}

module.exports.convertTemp = convertTemp
