const request = require('request');

var geocodeAddress = (address) => {
  return new Promise((resolve, reject) => {
    var addressURI = encodeURIComponent(address)

    request({
      url: `http://maps.googleapis.com/maps/api/geocode/json?address=${addressURI}`,
      json: true
    }, (error, response, body) => {
      // console.log(response);
      if (error) {
        reject('unable to connect to google maps api.');
      } else if (body.status === 'ZERO_RESULTS') {
        reject('address unknown.');
      } else if (body.status === 'OK') {
        resolve(body)
      } else if (response.status === 'OVER_QUERY_LIMIT') {
        reject('You have reached google map API hit limit.');
      }
      reject('unknown error')
    })
  })
}

geocodeAddress('as;dfj84f').then((location) => {
  console.log(JSON.stringify(location, undefined, 2));
}, (errorMsg) => {
  console.log(errorMsg);
})
