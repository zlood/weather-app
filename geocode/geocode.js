const request = require('request');

var geocodeAddress = (address, callback) => {
  var addressURI = encodeURIComponent(address)

  request({
    url: `http://maps.googleapis.com/maps/api/geocode/json?address=${addressURI}`,
    json: true
  }, (error, response, body) => {
    // console.log(response);
    if (error) {
      callback('unable to connect to google maps api.');
    } else if (body.status === 'ZERO_RESULTS') {
      callback('address unknown.');
    } else if (body.status === 'OK') {
      callback(undefined, {
        address: body.results[0].formatted_address,
        latitude: body.results[0].geometry.location.lat,
        longitude: body.results[0].geometry.location.lng
      })
    } else if (response.status === 'OVER_QUERY_LIMIT') {
      callback('You have reached google map API hit limit.');
    }
  })
}

module.exports.geocodeAddress = geocodeAddress;
