const yargs = require('yargs');
const geocode = require('./geocode/geocode');
const weather = require('./weather/weather');

const argv = yargs
  .options({
    a: {
      demand: true,
      alias: 'address',
      describe: 'Address you are looking for',
      string: true
    }
  })
  .help()
  .alias('help','h')
  .argv;

geocode.geocodeAddress(argv.a, (geocodeErrorMsg, gecodeResults) =>{
  if (geocodeErrorMsg) {
    console.log(geocodeErrorMsg);
  } else {
    weather.getWeather(gecodeResults.latitude, gecodeResults.longitude, (weatherErrorMsg, weatherResults) => {
      if (weatherErrorMsg) {
        console.log(weatherErrorMsg);
      } else {
        console.log(`current temperature in ${gecodeResults.address} is ${weatherResults.temperature}C. But it feels like ${weatherResults.actualTemperature}C`);
      }
    });
  }
});
